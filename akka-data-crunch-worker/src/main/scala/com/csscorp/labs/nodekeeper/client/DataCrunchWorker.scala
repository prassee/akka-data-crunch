package com.csscorp.labs.nodekeeper.client

import scala.collection.mutable.Queue
import scala.collection.mutable.ListBuffer
import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.ActorSystem
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.pattern.ask
import akka.util.Timeout
import java.net.URL
import akka.actor.Actor
import com.csscorp.labs.nodekeeper.Commons._
import com.csscorp.labs.naaas.notation.api.LineFeedExtractor

/*
 * Workers are those which do heavy lifting of 
 * crunching the data in to fields/tuples as 
 * consumed by the downstream systems. The
 * data once crunch it sends to the tuples stream. 
 */
class DataCrunchWorker extends Actor {

  val master = context.actorFor("akka://DaaasDataCrunchSystem@127.0.0.1:2554/user/DataCrunchMaster")
  val streamer = context.actorFor("akka://DaaasDataCrunchSystem@127.0.0.1:2554/user/SourceFieldStreamer")
  implicit val ec = context.dispatcher

  case object HeartBeat

  //scheduler to send heart-beat
  val hbTicker = context.system.scheduler.schedule(0.millis, 500.millis,
    self, HeartBeat)

  def receive = {

    case HeartBeat => {
      master.!("uod")
    }

    case UnitOfData(jobId: String, srcId: String, data: String) => {
      // call to job store to get src field mappings for srcId

      // call to object notation api 
      val dc = LineFeedExtractor(data)

      // send the data back to stream actor
      streamer.!(Fields(data)) 
    }

    /*
     * this tells the worker to stop
     * asking for data from the master
     */
    case false => {
      hbTicker.cancel()
    }
  }
}