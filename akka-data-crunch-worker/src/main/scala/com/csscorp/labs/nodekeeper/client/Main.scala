package com.csscorp.labs.nodekeeper.client

import akka.actor.ActorSystem
import akka.actor.Props

object Main extends App{
  val system = ActorSystem("DaaasDataCrunchSystem")
  val dcm = system.actorOf(Props[DataCrunchWorker], name = "DataCrunchWorker")
}