package com.csscorp.labs.nodekeeper

object Commons {
  case class UnitOfData(jobId: String, srcId: String, data: String)
  case class Fields(data:String)
}