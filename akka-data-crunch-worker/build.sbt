name := "naaas-data-crunch-worker"
 
version := "1.0"
 
scalaVersion := "2.10.0"
 
resolvers += "csslabs internal repo" at "http://10.8.27.13:9000/artifactory/repo/"
 
libraryDependencies ++= Seq("com.typesafe.akka" % "akka-actor_2.10" % "2.1.2",
"com.typesafe.akka" % "akka-remote_2.10" % "2.1.2",
"com.typesafe.akka" % "akka-slf4j_2.10" % "2.1.2",
"naaas-object-notation"% "naaas-object-notation" % "1.0.0")	
