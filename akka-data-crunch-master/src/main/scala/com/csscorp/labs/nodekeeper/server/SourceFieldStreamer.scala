package com.csscorp.labs.nodekeeper.server

import akka.actor.Actor
import scala.collection.mutable.Queue
import com.csscorp.labs.nodekeeper.Commons.Fields

/*
 * the class used by the API to stream out the 
 * tuples 
 */
class SourceFieldStreamer extends Actor {
  var msgQ = new Queue[String]
  val max = 500
  def receive() = {

    case Fields(data) => {
      if (msgQ.length < max) {
        println("obtained fields " + data)
        msgQ = msgQ.+=("fields")
      }
    }

    case "stream" => {
      sender.!(msgQ.dequeue())
    }

  }

}