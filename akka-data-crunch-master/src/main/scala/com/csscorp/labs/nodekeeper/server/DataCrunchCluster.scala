package com.csscorp.labs.nodekeeper.server

import akka.actor.ActorSystem
import akka.actor.Props
import akka.actor.actorRef2Scala
import com.csscorp.labs.nodekeeper.Commons._
import java.util.Date
import akka.actor.ActorRef
import akka.util.Timeout
import akka.pattern.ask
import scala.concurrent.duration._
import scala.concurrent.Await

object DataCrunchCluster {
  // create the system and actor
  val system = ActorSystem("DaaasDataCrunchSystem")
  val dcm = system.actorOf(Props[DataCrunchMaster], name = "DataCrunchMaster")
  val streamer = system.actorOf(Props[SourceFieldStreamer], name = "SourceFieldStreamer")
  def apply() = new DataCrunchCluster(dcm, streamer)
}

class DataCrunchCluster(dcm: ActorRef, streamer: ActorRef) {
  
  def sendDataToCrunch(uod: UnitOfData) {
    dcm.!(uod)
  }

  def getCrunchedData(jobId: String) = {
    implicit val timeout = Timeout(5 seconds)
    val future = streamer.?("stream")
    val result = Await.result(future, timeout.duration).asInstanceOf[String]
    result
  }
}

