package com.csscorp.labs.nodekeeper.server

import scala.io.Source
import com.csscorp.labs.nodekeeper.Commons.UnitOfData

object DataCrunchSample extends App {

  val dc = DataCrunchCluster()

  val src = Source.fromFile("/home/prasanna/dump.log")
  val x = src.getLines().foreach(x => {
    dc.sendDataToCrunch(UnitOfData("", "", x))    
  })
}