package com.csscorp.labs.nodekeeper.server

import scala.collection.mutable.Queue
import akka.actor.Actor
import com.csscorp.labs.nodekeeper.Commons._

class DataCrunchMaster extends Actor {
  var msgQ = new Queue[UnitOfData]
  val max = 50
  
  def receive = {
    case UnitOfData(jobId, srcId, data) => {
      if (msgQ.length < max) {
        msgQ = msgQ.+=(UnitOfData(jobId, srcId, data))
      }
    }

    case "uod" => {
      if(!msgQ.isEmpty) {
        val tup = msgQ.dequeue
        sender.!(UnitOfData(tup.jobId, tup.srcId, tup.data))
      } else {
        sender.!(false)
      }
    }
  }
}