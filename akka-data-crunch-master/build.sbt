name := "naaas-data-crunch-master"
 
version := "1.0"
 
scalaVersion := "2.10.0"
 
resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"
 
libraryDependencies ++= Seq("com.typesafe.akka" % "akka-actor_2.10" % "2.1.2",
"com.typesafe.akka" % "akka-remote_2.10" % "2.1.2",
"com.typesafe.akka" % "akka-slf4j_2.10" % "2.1.2")	
